# The Chaos by Gerard Nolst Trenité

## Build

```
$ latexmk -norc -lualatex -interaction=nonstopmode poem.tex
```

## Links

* http://ncf.idallen.com/english.html
* http://ncf.idallen.com/TheChaosPRETTY.pdf
* https://en.wikisource.org/wiki/Ruize-rijmen/De_Chaos

------

Typeset by Leonard König, contribute here:
https://gitlab.com/LeonardK/the_chaos_poem
